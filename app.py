import random
import sys

from flask import Flask, render_template

app = Flask(__name__)


cat_tags = [
    'cute',
    'orange',
    'fat',
    'small',
    'big',
    'kitty',
    'derp'
]


@app.route('/')
def index():
    return render_template('index.html', tag=random.choice(cat_tags))


if __name__ == '__main__':
    app.run(host=sys.argv[1], port=int(sys.argv[2]), debug=True)